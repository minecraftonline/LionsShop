package com.minecraftonline.lionsshop.signs;

import com.flowpowered.math.vector.Vector3i;
import com.minecraftonline.lionsshop.LionsShopSign;
import com.sk89q.craftbook.sponge.util.SignUtil;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.List;

public class SlotInfo implements LionsShopSign {
    private static final String TITLE = "[SlotInfo]";
    static final Vector3i fromSlotCtrl = new Vector3i(0,1,0);

    private Location<World> location;

    private SlotInfo(Location<World> loc) {
        this.location = loc;
    }

    @Override
    public void onRightClick(Player player) {}

    void increment(int lineNum) {
        List<Text> lines = location.require(Keys.SIGN_LINES);
        int num = Integer.parseInt(lines.get(lineNum).toPlain());
        num++;
        int totalGames = Integer.parseInt(lines.get(3).toPlain());
        totalGames++;
        lines.set(lineNum, Text.of(num));
        lines.set(3, Text.of(totalGames));
        location.offer(Keys.SIGN_LINES, lines);
    }

    public static class Factory implements LionsShopSign.Factory<SlotInfo> {

        @NonNull
        @Override
        public SlotInfo create(Location<World> signLoc, SignData data, Player player) {
            if (!matches(data)) {
                throw new RuntimeException("sign passed into create of SlotInfo was not a slotinfo sign!");
            }
            if (!SignUtil.getTextRaw(data, 0).equals(TITLE)) {
                // Fix casing
                data.setElement(0, Text.of(TITLE));
            }
            data.setElement(1, Text.of(0));
            data.setElement(2, Text.of(0));
            data.setElement(3, Text.of(0));
            return new SlotInfo(signLoc);
        }

        @NonNull
        @Override
        public SlotInfo load(Sign sign) {
            if (!matches(sign.getSignData())) {
                throw new RuntimeException("sign passed into load of SlotInfo was not a slotinfo sign!");
            }
            return new SlotInfo(sign.getLocation());
        }

        @Override
        public boolean matches(SignData signData) {
            return SignUtil.getTextRaw(signData, 0).equalsIgnoreCase(TITLE);
        }

        @Override
        public boolean isInteractable() {
            return false;
        }
    }
}
