package com.minecraftonline.lionsshop.commands;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.util.blockray.BlockRay;
import org.spongepowered.api.util.blockray.BlockRayHit;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.UUID;

public class UpgradeSign implements CommandExecutor // Upgrades sign from 1.7.10 lionshop to current version
{
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException
    {
        UUID uuid = null;
        if (args.<String>getOne("uuid").get().length() == 36)
            uuid = UUID.fromString(args.<String>getOne("uuid").get());

        Player player = (Player) src;
        BlockRay<World> blockray = BlockRay.from(player).build();
        BlockRayHit<World> hitOpt = blockray.next();
        for (int i = 0; i < 9; i++) // number of blocks to check for a sign, (-1 off total since next() is called the line before
        {
            if (hitOpt.getLocation().getBlockType().equals(BlockTypes.WALL_SIGN))
                break;
            hitOpt = blockray.next();
        }
        if (!hitOpt.getLocation().getBlockType().equals(BlockTypes.WALL_SIGN))
        {
            throw new CommandException(Text.of("No sign found"));
        }
        Sign sign = (Sign) hitOpt.getLocation().getTileEntity().get();
        if (!sign.lines().get(0).toPlain().equals("[Shop]") && !sign.lines().get(0).toPlain().equals("[SlotCtrl]"))
        {
            throw new CommandException(Text.of("Sign is not a shop/slot sign"));
        }
        if (uuid != null)
        {
            hitOpt.getExtent().setCreator(hitOpt.getBlockPosition(), uuid);
            sign.offer(sign.lines().set(3,Text.of("")));
        }
        else
        {
            if (!Sponge.getServer().getPlayer(args.<String>getOne("uuid").get()).isPresent())
                throw new CommandException(Text.of("Player is not online, use the UUID instead"));
            uuid = Sponge.getServer().getPlayer(args.<String>getOne("uuid").get()).get().getUniqueId();
            hitOpt.getExtent().setCreator(hitOpt.getBlockPosition(), uuid);
            sign.offer(sign.lines().set(3,Text.of("")));
        }
        if (src instanceof Player)
            src.sendMessage(Text.of("Successfully updated owner"));

        return CommandResult.success();
    }
}