package com.minecraftonline.lionsshop.commands;

import com.minecraftonline.lionsshop.LionsShopUtil;
import com.minecraftonline.util.IDUtil;
import com.sk89q.craftbook.sponge.util.prompt.ItemStackSnapshotDataPrompt;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.util.blockray.BlockRay;
import org.spongepowered.api.util.blockray.BlockRayHit;
import org.spongepowered.api.world.World;
import org.spongepowered.api.world.extent.Extent;

import java.util.Optional;

public class EditSign implements CommandExecutor
{
    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

        String buySell = args.requireOne(Text.of("buysell"));

        int amount = args.requireOne("amount");

        Optional<Integer> item = args.getOne("item");

        if (!(amount > 0))
            throw new CommandException(Text.of("Amount must be above 0"));
        if (amount > 2304) {
            throw new CommandException(Text.of("Amount must be less than or equal to 2304"));
        }

        if (!(src instanceof Player))
        {
            throw new CommandException(Text.of("Must be called from a player"));
        }
        Player player = (Player) src;

        BlockRay<World> blockRay = BlockRay.from(player).build();
        BlockRayHit<World> hitOpt = blockRay.next();
        for (int i = 0; i < 9; i++) // number of blocks to check for a sign, (-1 off total since next() is called the line before
        {
            if (hitOpt.getLocation().getBlockType().equals(BlockTypes.WALL_SIGN))
                break;
            hitOpt = blockRay.next();
        }
        if (!hitOpt.getLocation().getBlockType().equals(BlockTypes.WALL_SIGN))
        {
            throw new CommandException(Text.of("No sign found after search"));
        }

        Sign sign = (Sign)hitOpt.getLocation().getTileEntity().get();

        if (!sign.lines().get(0).toPlain().equals("[Shop]"))
        {
            throw new CommandException(Text.of("Sign is not a shop sign"));
        }
        if (!hitOpt.getLocation().createSnapshot().getCreator().isPresent())
        {
            throw new CommandException(Text.of("Sign is outdated, right click to update it"));
        }
        if (!hitOpt.getLocation().createSnapshot().getCreator().get().toString().equals(player.getUniqueId().toString()) && !player.hasPermission("shop.admin"))
        {
            throw new CommandException(Text.of("You do not own this shop (" + hitOpt.getLocation().createSnapshot().getCreator().get() + "), owns it"));
        }

        boolean buy = buySell.equals("buy");

        if (item.isPresent()) {
            editShop(sign, buy, item.get(), amount, args.<Integer>getOne("damage").orElse(0));
            return CommandResult.success();
        }

        ItemStackSnapshotDataPrompt dataPrompt = new ItemStackSnapshotDataPrompt(1, 1, "Enter Items");

        dataPrompt.getData(player, callback ->
        {
            Entity summonItem;
            Extent extent = player.getLocation().getExtent();
            for (ItemStackSnapshot itemSnapshot : callback)
            {
                summonItem = extent.createEntity(EntityTypes.ITEM, player.getLocation().getPosition());
                summonItem.offer(Keys.REPRESENTED_ITEM, itemSnapshot);
                LionsShopUtil.setOwner(summonItem, player);
                player.getLocation().spawnEntity(summonItem);
            }
            int dmgval = (int) callback.get(0).toContainer().get(DataQuery.of("UnsafeDamage")).get();
            if (callback.get(0).getType().equals(ItemTypes.FILLED_MAP))
                dmgval = 0; // dont include dmg value for maps

            int legacyId = IDUtil.getLegacyIDFromItemStack(callback.get(0).getType());

            editShop(sign, buy, legacyId, amount, dmgval);
        });

        return CommandResult.success();
    }

    private static void editShop(Sign sign, boolean buy, int itemId, int amount, int dmg) {
        String line = itemId + " " + amount;
        if (dmg != 0) {
            line = line + " " + dmg;
        }
        int lineNum = buy ? 1 : 2;
        sign.offer(sign.lines().set(lineNum, Text.of(line)));
    }
}