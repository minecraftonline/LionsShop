package com.minecraftonline.lionsshop;

import com.minecraftonline.lionsshop.exceptions.InvalidSignCreationException;
import com.minecraftonline.lionsshop.exceptions.InvalidSignStateException;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.data.manipulator.mutable.tileentity.SignData;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

/**
 * Implemented by all sign types in LionsShop, eg. Shop, ShopInfo, SlotCtrl, SlotInfo etc
 */
public interface LionsShopSign {
    /**
     * Called when a player right clicks the sign
     * @param player who clicked the sign
     */
    void onRightClick(Player player);

    interface Factory<T extends LionsShopSign> {
        /**
         * Called when player first places down a sign
         * @param signLoc where the sign was placed
         * @param player who placed it
         * @throws InvalidSignCreationException if there is an issue with placement and event should be cancelled
         * @return LionsShopSign
         */
        @NonNull
        T create(Location<World> signLoc, SignData data, Player player) throws InvalidSignCreationException;

        /**
         * Used to load a sign for actions
         * @param sign to create instance from
         * @throws InvalidSignStateException if the sign state is no longer valid
         * @return LionsShopSign
         */
        @NonNull
        T load(Sign sign) throws InvalidSignStateException;
        /**
         * If the sign to be checked is of this type
         * @param signData to be checked
         * @return boolean if the sign is of this type
         */
        boolean matches(SignData signData);

        /**
         * Checks if the sign is interactable
         * @return if onRightClick should be called
         */
        boolean isInteractable();
    }
}
