package com.minecraftonline.lionsshop.exceptions;

public class InvalidSignStateException extends Exception {
    public InvalidSignStateException(String msg) {
        super(msg);
    }

    public InvalidSignStateException(String msg, Throwable t) {
        super(msg, t);
    }
}
