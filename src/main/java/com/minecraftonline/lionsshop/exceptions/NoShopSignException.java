package com.minecraftonline.lionsshop.exceptions;

public class NoShopSignException extends InvalidSignCreationException {
    public NoShopSignException(String msg) {
        super(msg);
    }
}
