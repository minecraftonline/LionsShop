package com.minecraftonline.lionsshop.exceptions;

public class InvalidSignCreationException extends Exception {
    public InvalidSignCreationException(String msg) {
        super(msg);
    }
    public InvalidSignCreationException(String msg, Exception e) {
        super(msg, e);
    }
}
