package com.minecraftonline.lionsshop;

import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;

import java.util.Random;

public class SlotRolls {
    private static final Random dice = new Random();
    private static final int[][] reels =
            {
                    {1, 1, 1},
                    {1, 1, 6},
                    {1, 6, 1},
                    {5, 5, 5},
                    {6, 6, 5},
                    {2, 2, 5},
                    {2, 2, 2},
                    {2, 2, 1},
                    {2, 4, 3},
                    {4, 3, 3},
                    {3, 3, 3},
                    {3, 3, 5},
                    {3, 3, 4},
                    {5, 5, 5},
                    {6, 6, 4},
                    {4, 6, 4},
                    {4, 4, 4},
                    {4, 4, 4},
                    {4, 4, 6},
                    {6, 6, 5},
                    {6, 5, 5},
                    {5, 5, 5},
                    {5, 5, 5},
                    {5, 5, 5},
                    {5, 5, 6},
                    {6, 6, 6},
                    {5, 5, 5},
                    {4, 5, 5},
                    {4, 4, 5},
                    {1, 1, 1},
                    {0, 0, 4},
                    {0, 0, 0},
                    {0, 0, 4},
                    {0, 6, 6},
                    {2, 2, 5},
                    {2, 2, 3},
                    {2, 3, 3},
                    {1, 4, 6},
                    {1, 1, 6},
                    {1, 1, 1},
                    {1, 5, 5},
                    {6, 1, 2},
                    {0, 2, 2},
                    {2, 2, 5},
                    {2, 5, 5},
                    {5, 5, 4},
                    {5, 4, 4},
                    {4, 4, 3},
                    {3, 3, 3},
                    {3, 3, 2},
                    {3, 3, 6},
                    {3, 6, 4},
                    {6, 4, 4},
                    {4, 4, 4},
                    {4, 4, 4},
                    {4, 3, 3},
                    {3, 3, 3},
                    {3, 3, 5},
                    {5, 5, 5},
                    {5, 5, 5},
                    {5, 5, 5},
                    {5, 1, 5},
                    {6, 1, 4},
                    {3, 4, 5}
            };
    public static int[] roll() {
        return new int[] {
                rollOne()[0],
                rollOne()[1],
                rollOne()[2]
        };
    }
    public static int[] rollOne() {
        return reels[dice.nextInt(reels.length)];
    }
    public static DyeColor diceRollToColor(int roll) {
        switch(roll)
        {
            case 5:
                return DyeColors.ORANGE;
            case 4:
                return DyeColors.LIME;
            case 3:
                return DyeColors.CYAN;
            case 2:
                return DyeColors.PINK;
            case 1:
                return DyeColors.YELLOW;
            case 0:
                return DyeColors.RED;
        }
        return DyeColors.WHITE;
    }

    public static int getPrize(int left, int middle, int right) {
        int prize = 0;

        if(left == middle && left == right)
        {
            switch(left)
            {
                case 0: // RED
                    prize = 64;
                    break;
                case 1: // YELLOW
                    prize = 40;
                    break;
                case 2: // PINK
                    prize = 25;
                    break;
                case 3: // CYAN
                    prize = 10;
                    break;
                case 4: // LIME
                    prize = 7;
                    break;
                case 5: // ORANGE
                    prize = 5;
                    break;
            }
        }
        else
        {
            // Pair
            if(left == middle || middle == right)
            {
                if(middle == 5) // ORANGE
                    prize = 2;
                if(middle == 4) // LIME
                    prize = 3;
            }
            else
            {
                // Single
                if(left == 5)
                {
                    prize = 1;
                }
            }
        }
        return prize;
    }
}
