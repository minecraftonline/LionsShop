package com.minecraftonline.lionsshop;

import com.google.inject.Inject;
import com.minecraftonline.lionsshop.commands.EditServerShopSign;
import com.minecraftonline.lionsshop.commands.EditSign;
import com.minecraftonline.lionsshop.commands.UpgradeSign;
import com.minecraftonline.lionsshop.signs.*;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.service.permission.*;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.command.spec.CommandSpec;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Adds sign shops and slot machines to minecraft
 * @author tyhdefu
 * @author bastetfurry (Legacy) - ideas and Slot machine probabilities
 */

@Plugin(id = "lionsshop", name = "LionsShop")
public class LionsShop {

    private static Logger logger;

    private static LionsShop instance;

    @Inject
    public LionsShop(Logger logger)
    {
        LionsShop.logger = logger;
        instance = this;
    }

    public static LionsShop getInstance() {return instance;}
    public static Logger getLogger() {return LionsShop.logger;}

    @Listener
    public void onServerStart(GameStartedServerEvent event) {
        List<LionsShopSign.Factory> signs = new ArrayList<>();
        signs.add(new Shop.Factory());
        signs.add(new ServerShop.Factory());
        signs.add(new ShopInfo.Factory());
        signs.add(new ShopContent.Factory());
        signs.add(new SlotCtrl.Factory());
        signs.add(new SlotInfo.Factory());
        Sponge.getEventManager().registerListeners(this, new lsSignListener(signs));

        Map<String, String> arg1 = new HashMap<String, String>(){{put("buy","buy");put("sell","sell");}};

        PermissionService permissionService = Sponge.getServiceManager().provide(PermissionService.class).get();

        permissionService.newDescriptionBuilder(this)
                .id("lionsshop.admin.upgrade")
                .description(Text.of("Allows user to run /lssignupgrade"))
                .register();

        permissionService.newDescriptionBuilder(this)
                .id("lionsshop.shop.create")
                .description(Text.of("Allows user to create shops"))
                .register();

        permissionService.newDescriptionBuilder(this)
                .id("lionsshop.editsign")
                .description(Text.of("Allows user to do /lseditsign"))
                .register();

        permissionService.newDescriptionBuilder(this)
                .id("lionsshop.createslots")
                .description(Text.of("Allows user to create slots"))
                .register();

        CommandSpec editShopCommand = CommandSpec.builder()
                .description(Text.of("Edit shop signs"))
                .arguments(
                        GenericArguments.onlyOne(GenericArguments.choicesInsensitive(Text.of("buysell"), arg1)),
                        GenericArguments.integer(Text.of("amount")),
                        GenericArguments.optional(GenericArguments.integer(Text.of("item"))),
                        GenericArguments.optional(GenericArguments.integer(Text.of("damage")))
                )
                .permission("lionsshop.editsign")
                .executor(new EditSign()).build();

        CommandSpec upgradeShopCommand = CommandSpec.builder()
                .description(Text.of("Upgrade shop sign to the new version"))
                .arguments(
                        GenericArguments.string(Text.of("uuid")))
                .permission("lionsshop.admin.upgrade")
                .executor(new UpgradeSign()).build();

        CommandSpec editServerShopCommand = CommandSpec.builder()
                .description(Text.of("Edit server shop signs."))
                .arguments(
                        GenericArguments.onlyOne(GenericArguments.choicesInsensitive(Text.of("buysell"), arg1)),
                        GenericArguments.integer(Text.of("amount")),
                        GenericArguments.optional(GenericArguments.integer(Text.of("item"))),
                        GenericArguments.optional(GenericArguments.integer(Text.of("damage")))
                )
                .permission("lionsshop.servershop")
                .executor(new EditServerShopSign())
                .build();

        Sponge.getCommandManager().register(this, editShopCommand, "lsshopedit");
        Sponge.getCommandManager().register(this, editServerShopCommand, "lsservershopedit");
        Sponge.getCommandManager().register(this, upgradeShopCommand, "lsignupgrade");
    }
}


