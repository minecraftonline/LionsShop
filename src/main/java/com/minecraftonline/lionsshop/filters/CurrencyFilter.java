package com.minecraftonline.lionsshop.filters;

import com.minecraftonline.lionsshop.LionsShopUtil;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.ItemStack;

import java.util.function.Predicate;

public class CurrencyFilter implements Predicate<ItemStack> {
    private final int dmg;
    private final ItemType type;

    public CurrencyFilter(ItemType type, int dmg) {
        this.dmg = dmg;
        this.type = type;
    }

    @Override
    public boolean test(ItemStack itemStack) {
        return !LionsShopUtil.hasNbt(itemStack) // Currency shouldn't have nbt.
                && LionsShopUtil.isEqualIgnoringNBT(itemStack, type, dmg);
    }
}
